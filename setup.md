---
title: Setup
---

## Podman installation

If you want to install Podman on your local system, please follow the installation guide for your
Operating System below.

### Linux

Please refer to the
[Installation Guide for Podman Desktop for Linux](https://podman-desktop.io/docs/installation/linux-install)
for up-to-date installation instructions.
Make sure that you also install Podman itself according to the
[installation instructions](https://podman.io/docs/installation#installing-on-linux)
beforehand.

### Mac OS

Please refer to the
[Installation Guide for MacOS](https://podman-desktop.io/docs/installation/macos-install)
for up-to-date installation instructions.
Make sure that you also install Podman itself according to the
[installation instructions](https://podman.io/docs/installation#macos)
beforehand.

### Windows

Please refer to the
[Installation Guide for Windows](https://podman-desktop.io/docs/installation/windows-install)
for up-to-date installation instructions.
Podman itself is then already part of the installation.

### Verify your Installation

If you successfully installed Podman on your system, you can test your installation by running the
following command on the command-line:

~~~
$ podman run hello-world
Hello from Docker!
This message shows that your installation appears to be working correctly.
~~~
{: .language-terminal}

## Apptainer/Singularity Installation

Apptainer/Singularity is currently available for Linux-based OS. Please read the
[Installation Instructions](https://apptainer.org/docs/admin/main/installation.html)

{% include links.md %}
