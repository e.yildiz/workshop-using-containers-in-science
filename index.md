---
layout: lesson
root: .  # Is the only page that doesn't follow the pattern /:path/index.html
permalink: index.html  # Is the only page that doesn't follow the pattern /:path/index.html
---
<!-- this is an html comment -->

{% comment %} This is a comment in Liquid {% endcomment %}

A container is a standard unit of software that packages up code and all its dependencies so the
application runs quickly and reliably from one computing environment to another. A container image
is a lightweight, standalone and executable package of software that includes everything to run
an application. Two popular container solutions are [Podman](https://podman-desktop.io/) and
[Apptainer](https://apptainer.org/) (formerly known as *Singularity*).

In this session you will learn how to use containers on your local system or on an HPC system. At
the end of the course you will also be able to package your own application into your custom
container. By uploading the container image to a registry, you learn how to share your work with
others and simplify the distribution and increase the portability of your software.

> ## Prerequisites
>
> In this course we use Podman and Apptainer from the Unix Shell. Some previous experience with
> the shell is expected.
{: .prereq}

{% include links.md %}
