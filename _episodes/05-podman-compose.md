---
title: "Using Podman Compose"
teaching: -1
exercises: -1
questions:
- "What is Podman Compose?"
- "How can I use file-based container settings?"
- "How do I run multiple containers at once?"
objectives:
- "Create and use a Podman Compose file."
- "Run multiple (interacting) containers at once."
keypoints:
- "Using Podman Compose makes developing and using containerized applications easier most of the time"
- "Use a `docker-compose.yml` to configure your container(s)"
- "Use `podman-compose up -d` to start your container(s)"
- "Use `podman-compose down` to stop your container(s)"
---

## Brief Introduction to Podman Compose

Sometimes your settings for a container become more advanced.
Sometimes you want to run multiple containers at once.
These are tasks, where Podman Compose makes developing and using containerized applications easier.

## Verify the Podman Compose installation

Podman Compose is not part of the Podman Desktop installation.

To install Podman Compose, please follow the
[installation guide](https://github.com/containers/podman-compose#installation).
The recommended way for Ubuntu Linux is:

> ~~~
> $ pip install podman-compose
> ~~~
> {: .language-terminal}

To verify your installation via `pip`, use the following command:

> ~~~
> $ podman-compose version
> podman compose version: 1.0.6
> ['podman', '--version', '']
> using podman version: 3.4.2
> podman compose version 1.0.6
> podman --version
> podman version 3.4.2
> exit code: 0
> ~~~
> {: .language-terminal}

## What is Podman Compose?

From the official wiki:

```text
Compose is a tool for defining and running multi-container [...] applications.
With Compose, you use a YAML file to configure your application’s services.
Then, with a single command, you create and start all the services from your configuration.
```

Additionally, it is a good way to store container configurations (in a file).

## Why do we need Podman Compose?

More advanced Podman configurations might be unwieldy to use with only Podman.
You may have multiple Dockerfiles for your multi-container application or complicated setups in general.
This is where Podman Compose can help you tremendously.
There will be a single YAML file that describes your setup and with a simple call of Podman Compose you are able to spin up your whole application with ease.

A complex Podman call like this

~~~
$ podman run -d \
         --name=statping \
         -p 8080:8080 \
         --restart unless-stopped \
         docker.io/statping/statping
~~~
{: .language-terminal}

becomes this

~~~
$ podman-compose up -d
~~~

As already mentioned above, we need to write our configuration to a YAML file called `docker-compose.yml`
to make this possible.
Every setting from the Podman call needs to be put into that file to achieve the same result.

```yaml
version: "3.9"
services:
  statping:
    image: docker.io/statping/statping
    ports:
      - "8080:8080"
    restart: unless-stopped
```

## Writing a `docker-compose.yml`

Podman Compose implements the [Compose Spec](https://compose-spec.io/) with a Podman backend.
We see some key-value pairs in this YAML file.
We will go through them individually, while building the following example:

> **Note:** We will use a web application, because it is (for this example) easier to set up as a demo.

```shell
cd $PRJECTDIR
$EDITOR docker-compose.yml
```

```yaml
version: "3.9"

services:
  db:
    image: docker.io/library/mysql:5.7
    volumes:
      - db_data:/var/lib/mysql
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: somewordpress
      MYSQL_DATABASE: wordpress
      MYSQL_USER: wordpress
      MYSQL_PASSWORD: wordpress

  wordpress:
    depends_on:
      - db
    image: docker.io/library/wordpress:latest
    ports:
      - "8000:80"
    restart: unless-stopped
    environment:
      WORDPRESS_DB_HOST: db:3306
      WORDPRESS_DB_USER: wordpress
      WORDPRESS_DB_PASSWORD: wordpress
      WORDPRESS_DB_NAME: wordpress

volumes:
  db_data: {}
```

---

**version**

Provides information about the version of the Compose YAML.
It is optional and mainly needed to support older Compose clients.

**services**

Within this key all containers managed by Compose are listed.
Only list containers that are related.
Every project should have its own `docker-compose.yml`

**$SERVICENAME**

Gives every service a unique name.
There are some limitations (e.g. no colons) to the name.
Within a given service, all settings for this service are stored.

**image**

Defines the image to use.
Only images from registries like Docker Hub, Quay.io or GitLab Container Registry can be used here.

**build**

In case you build your own image, use this key.
You can define a `context` to specify a working directory during build.
Additionally, you can point to a Dockerfile to use.

```yaml
services:
  backend:
    build:
      context: ./backend/api
      dockerfile: ./podman/Dockerfile
  frontend:
    build:
      context: ./frontend
      dockerfile: ./podman/Dockerfile
```

**volumes**

Defines a list, which specifies files and folders to persist beyond the lifetime of the containers and specifies persisted files and folders which should be mounted within the container.

**restart**

Defines the restart policy.

**environment**

Set environment variables within the container.
This is done during the start of the container.

**depends-on**

Sets one (ore more) services as dependency for another service.
It will only start, if the dependency started successfully.

**ports**

Defines a list, which ports should be routed to the host.

---

The list above is far from being complete.
It is possible to create very complex structures within Compose.

> **Note:** some IDEs can auto-complete and lint `docker-compose.yml` files (JetBrains IDEs, for example)

## Starting Compose

To start the containers, run

```shell
podman-compose up -d
```

You can now visit [localhost:8000](http://localhost:8000) to see the application running.

## Stopping Compose

```shell
podman-compose down
```
